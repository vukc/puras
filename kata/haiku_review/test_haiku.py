"""Test module for HaikuReview class"""

import unittest

from haiku import HaikuReview

class TestSyllableCounter(unittest.TestCase):
    """Test class for Syllable counter method"""

    def setUp(self):
        self.haiku = HaikuReview()

    def test_should_confirm_that_sentence_contains_1_syllable(self):
        """ Testing Scenario_1, sentence contains one syllable """
        self.assertEqual(self.haiku.count_syllables("adx"), 1)

    def test_should_confirm_that_sentence_contains_5_syllables(self):
        """ Testing Scenario_1, sentence contains five syllables """
        self.assertEqual(self.haiku.count_syllables("happy purple frog"), 5)

    def test_should_confirm_that_sentence_contains_7_syllables(self):
        """ Testing Scenario_1, sentence contains seven syllables """
        self.assertEqual(self.haiku.count_syllables("eating bugs in the marshes"), 7)

class TestHaikuCheck(unittest.TestCase):
    """ Test class for Haiku check method """

    def setUp(self):
        self.haiku = HaikuReview()

    def test_should_confirm_that_sentence_is_a_haiku(self):
        haiku = "happy purple frog/eating bugs in the marshes/get indigestion"
        self.assertTrue(self.haiku.check_haiku(haiku), True)

    def test_should_confirm_that_sentence_is_not_a_haiku(self):
        haiku = "computer programs/the bugs try to eat my code/i will not let them"
        self.assertFalse(self.haiku.check_haiku(haiku), False)

class TestPrintCheckResult(unittest.TestCase):
    """ Test class for Haiku print check result """

    def setUp(self):
        self.haiku = HaikuReview()

    def test_should_print_that_sentance_is_a_haiku(self):
        haiku = "happy purple frog/eating bugs in the marshes/get indigestion"
        self.assertEqual(self.haiku.print_haiku(haiku), "5,7,5,Yes")

    def test_should_print_that_sentance_is_not_a_haiku(self):
        haiku = "computer programs/the bugs try to eat my code/i will not let them"
        self.assertEqual(self.haiku.print_haiku(haiku), "5,8,5,No")

    def test_should_print_that_sentance_is_not_a_haiku(self):
        haiku = "sad grumpy toad/the rain keeps falling/soon will sun come"
        self.assertEqual(self.haiku.print_haiku(haiku), "4,5,5,No")



unittest.main()