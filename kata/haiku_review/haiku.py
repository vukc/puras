"""HaikuReview module"""

import re

class HaikuReview:
    """HaikuReview class"""

    def count_syllables(self, word):
        """ Scenario_1 - count syllables in the provided sentance """
        # Split the word into groups of consecutive vowels
        vowels = re.findall(r'[aeiouy]+', word)
        # Count the number of groups
        return len(vowels)

    def check_haiku(self, poem):
        """ Scenario_2 - verify if the sentance is a haiku """
        lines = poem.split('/')
        syllables = [sum(self.count_syllables(word) for word in line.split()) for line in lines]
        if syllables == [5, 7, 5]:
            return True
        else:
            return False

    def print_haiku(self, poem):
        """ Scenario_3 - print the check result """
        """Print a haiku and its syllable count in the required format."""
        lines = poem.split('/')
        syllables = [sum(self.count_syllables(word) for word in line.split()) for line in lines]
        value = self.check_haiku(poem)
        if value == True:
            return f"{syllables[0]},{syllables[1]},{syllables[2]},Yes"
        else:
            return f"{syllables[0]},{syllables[1]},{syllables[2]},No"