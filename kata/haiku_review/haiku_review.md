# Feature: Haiku Review

## Narrative:
---

**As a** customer <br>
**I want** to enter a poem and have it checked and verified <br>
**So that** I can be sure that the entered poem is in the format correct for a haiku <br>

---

## Scenario_1: Syllables are counted correctly

**Given**: A sentence is entered <br>
**When**: Number of syllables is counted in the sentence <br>
**Then**: The syllable count returns the number of syllables in the sentence <br>

## Scenario_2: Input is checked as a haiku

**Given**: A 3 sentence poem is entered <br>
**When**: Number of syllables is adequite for each sentence <br>
**Then**: Entered poem is confirmed as a haiku <br>

## Scenario_3: Displaying the haiku check result

**Given**: A 3 sentence poem is entered <br>
**When**: The input is checked as a haiku <br>
**Then**: Display the check result for the input <br>