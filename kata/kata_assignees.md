| Вежба                 | Име и презиме      | имејл                        | Број индекса |
| --------------------- | ------------------ | ---------------------------- | ------------ |
| 100 doors             | Nemanja Trifunović | nemanja.trifunovic@rt-rk.com | E240/2022    |
| 12 Days of Xmas       |                    |                              |              |
| ABC Problem           |                    |                              |              |
| Align Columns         |                    |                              |              |
| Anagrams              |                    |                              |              |
| Array Shuffle         |                    |                              |              |
| Balanced Parentheses  |                    |                              |              |
| Best Shuffle          |                    |                              |              |
| Bowling Game          |                    |                              |              |
| Calc Stats            |                    |                              |              |
| Closest To Zero       |                    |                              |              |
| Combined Number       |                    |                              |              |
| Count Coins           |                    |                              |              |
| Diff Selector         |                    |                              |              |
| Diversion             |                    |                              |              |
| Eight Queens          |                    |                              |              |
| Filename Range        |                    |                              |              |
| Fisher-Yates Shuffle  |                    |                              |              |
| Five Weekends         |                    |                              |              |
| Fizz Buzz             | Momcilo Krunic     | momcilo.krunic@labsoft.dev   | -            |
| Fizz Buzz Plus        |                    |                              |              |
| Friday 13th           |                    |                              |              |
| Game of Life          | Radomir Zlatkovic  | Radomir.Zlatkovic@rt-rk.com  | E247/2022    |
| Gray Code             | Stefan Marinkov    | marinkovstefan99@gmail.com   | E244/2022    |
| Group Neighbours      |                    |                              |              |
| Haiku Review          |                    |                              |              |
| Harry Potter          | Milica Vujanic     | Milica.Vujanic@rt-rk.com     | E258/2022    |
| ISBN                  |                    |                              |              |
| Knight's Tour         |                    |                              |              |
| LCD Digits            |                    |                              |              |
| Leap Years            | Luka Bilač         | luka.bilac@rt-rk.com         | E249/2022    |
| Levenshtein Distance  |                    |                              |              |
| Longest Common Prefix |                    |                              |              |
| Magic Square          |                    |                              |              |
| Mars Rover            |                    |                              |              |
| Mine Field            |                    |                              |              |
| Mine Sweeper          |                    |                              |              |
| Monty Hall            |                    |                              |              |
| Number Chains         |                    |                              |              |
| Number Names          |                    |                              |              |
| Phone Numbers         |                    |                              |              |
| Poker Hands           |                    |                              |              |
| Prime Factors         |                    |                              |              |
| Print Diamond         |                    |                              |              |
| Recently Used List    |                    |                              |              |
| Remove Duplicates     | Dušan Stanišić     | milenkozfk70@gmail.com       | E246/2022    |
| Reordering            |                    |                              |              |
| Reverse Roman         |                    |                              |              |
| Reversi               |                    |                              |              |
| Roman Numerals        |                    |                              |              |
| Saddle Points         |                    |                              |              |
| Tennis                |                    |                              |              |
| Tiny Maze             |                    |                              |              |
| Unsplice              |                    |                              |              |
| Vending Machine       |                    |                              |              |
| Wonderland Number     |                    |                              |              |
| Word Wrap             |                    |                              |              |
| Yatzy                 |                    |                              |              |
| Yatzy Cutdown         |                    |                              |              |
| Zeckendorf Number     |                    |                              |              |
